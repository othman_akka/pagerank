#______________________________________________ Libraires _______________________________________
import xml.etree.cElementTree as ET
from operator import add,mul,sub
from PIL import Image, ImageTk
from random import randint
from random import sample
import networkx as nx
import tkinter as tk
import pandas as pd
import pylab as plt
import numpy as np
import re

#______________________________________________ Gather links _______________________________________
def get_links():
    # open file
    file = open('dataset3.txt','r')
    text = file.readlines()
    file.close()
    
    arrayLinks = []
    # read line by line
    for line in text:
        #remove \n from line.split(' ')[2]
        arrayLinks.append(re.sub('\n','',line.split(' ')[2]))
        
    return arrayLinks

#______________________________________________ Create a file XML from links ________________________

# strategy of this method is linked each node with their random links
def create_xml():
    links = get_links()
    
    # create dict of each link
    dict_link = {}
    for link in links:
        # each link random for it some links 
        dict_link[link] = sample(links,randint(0, len(links)))
    
    graph = ET.Element("graph")
    for link in links:
        node = ET.SubElement(graph, "node" ,name=link)
        # bool on dict_link of link
        for index ,element in zip(range(len(links)),dict_link[link]):
            ET.SubElement(node, "link",name=str(index)).text = element
        
    tree = ET.ElementTree(graph)
    tree.write("graph1.xml",encoding="UTF-8",xml_declaration=True)

#______________________________________________ Create a file csv from xml file ______________________

def create_csv():
    array_links = []
    array_nodes = []
    
    #read file xml
    tree = ET.parse('graph1.xml')
    root = tree.getroot()
    
    for child in root:
        for child2 in child:
            # gather links of each node 
            array_links.append(child2.text)
            array_nodes.append(child.attrib['name'])
            
    df = pd.DataFrame({'node':array_nodes,'links':array_links})
    df.to_csv('links.csv',index=False,encoding='utf8')

def uris_node(array1,array2):
    return [link for link in array1 if link not in array2]

def verifiey(nodesSet , out_link , uris):
    if len(nodesSet) == len(out_link)+len(uris):
        return uris
    else:
        # save length of nodesSet equal length of sum out_link and uris
        a = abs(len(nodesSet)-(len(uris)+len(out_link)))
        return uris[:len(uris)-a]

#______________________________________________ Create list of each node with your links _______________

def node_links():
    # load a csv file
    data = pd.read_csv('links.csv', sep = ',')
    
    # array of nodes  
    nodes = data['node'].values
    
    # series of each links
    out_links = data['links']
    
    # index each link by its node
    out_links.index =nodes 
    
    # save just one of each node
    nodesSet = set(nodes)
    # array for each node and its links and no_links
    array_nodes = []
    
    for node in nodesSet:
        
        # if out link repeat several times so index this link just once
        if type(out_links.get(key=node)) is not str:
            # get uris of node that from differnce between all links and out-links
            uris = uris_node(nodesSet , out_links.get(key=node).values)

            # out-links
            out_link = out_links.get(key=node).values.tolist()
            
            # set array each a node and its out_links and uris
            uris = verifiey(nodesSet , out_link , uris)
        
            array_nodes.append([node , out_link , uris])

    return [array_nodes , nodesSet ,len(nodesSet)]

#______________________________________________ calculate a probabilite of a Teleportation _____________

def probability_D(node , links , out_links , N ,alpha):
    D = {}
    if len(out_links) == 0:
        for link in links:
            D[node+' -> '+link] = 1/N
    else :
        for link in links:
            D[node+' -> '+link] = (1-alpha)/N
    return D

#______________________________________________ calculate a probabilite of a Transportation _____________
def probability_T(node , links , out_links , N , alpha):
    T = {}
    for link in links:
        if link in out_links:
            T[node+' -> '+link] = (alpha/len(out_links))
        else:
            T[node+' -> '+link] = 0
    return T

#______________________________________________ calculate a probabilite P  ______________________________
def probability_matrix(matrix_graph , links , N ,alpha):
    # list of all probability of matrix_graph
    P = []
    for i in range(len(matrix_graph)):
        # node
        node = matrix_graph[i][0]
        # out_links
        out_links = matrix_graph[i][1]
        # uris
        #uris = matrix_graph[i][2]
        
        # probability of each a node
        D = probability_D(node , links , out_links , N , alpha)
        T = probability_T(node , links , out_links , N , alpha)
        
        # Simply an element-wise addition of two lists
        P.append(list(map(add,list(T.values()),list(D.values()))))
    return P

#______________________________________________ calculate a product of D and T ___________________________
def product_P_R(PS , R):
    PR = []
    for p in PS:
        # do multipe and sum of each elements of list
        PR.append(sum(list(map(mul,p,R))))
    return PR

#______________________________________________ implementation of page rank ______________________________           
def algo_page_rank():
    # Input
       # array for each node and its links , uri and length of pageRank 
    data = node_links()
       # matrix_graph 
    matrix_graph = data[0]
    
        # this method get nodes and its out_links as variable
    create_graph(matrix_graph)
    
       # all links
    links = data[1]
       # length of pages
    N = data[2]
    
       # damping factor
    alpha = 0.85
       # Pre-specified threshold
    epsilon = 0.0000000005
    
    ## Initialization
       # calculate the probability matrix
    P = probability_matrix(matrix_graph , links , N ,alpha)
       # PageRank vector
    PR_vector_next =[1/N for i in range(N) ]
    iteration = 0
    while(True):
        PR_vector_current = PR_vector_next
        
        PR_vector_next = product_P_R(P , PR_vector_current)
        iteration +=1
        # sub is operator of substraction that use with map for calcultion substraction between two lists
        if abs(sum(list(map(sub,PR_vector_next,PR_vector_current)))/N) <= epsilon :
            break
        
    #print('Iteration est :',iteration,' PageRank value est :',PR_vector_next)
    return [iteration , PR_vector_next]
#______________________________________________ interface of application ________________________________
def Interface(iteration , PR_vector_next):
    # create the root window
    window = tk.Tk() # create window
    window.configure(bg='lightgrey')
    window.title("Graph")
    window.geometry("680x570")
    
    # Create Menubar 
    menubar = tk.Menu(window) 
    
    # Adding File Menu and command of exit
    file = tk.Menu(menubar, tearoff = 0)
    menubar.add_cascade(label ='File', menu = file)
    file.add_command(label ='Exit', command = window.destroy)
    
    # size of picture for dimension in label
    width = 680
    height = 300
    
    # load a picture
    picture = Image.open("graph.png")
    
    # resize of the picture
    picture = picture.resize((width,height), Image.ANTIALIAS)
    
    # convert to PhotoImage
    picture = ImageTk.PhotoImage(picture)
    
    # labels 
    lbl0 = tk.Label(window, width=500, text="Graph", fg='black', font=("Helvetica", 16, "bold"))
    lbl1 = tk.Label(window,image = picture)
    lbl2 = tk.Label(window, width=500, text="console", fg='black', font=("Helvetica", 16,"bold"))
    
    # place of each label in window 
    lbl0.grid(row=0, column=0, sticky=tk.W)
    lbl1.grid(row=1, column=0, sticky=tk.W)
    lbl2.grid(row=2, column=0, sticky=tk.W)
    
    # create frame
    frame = tk.Frame(window)
    
    frame.grid(row=3, column=0, sticky=tk.W)
    
    # config row's frame
    window.rowconfigure(0, weight=0)
    
    # config column's frame
    window.columnconfigure(0, weight=1)
    
    #create scrollbar
    scrollbar = tk.Scrollbar(frame, orient="vertical")
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    
    # create listNodes as console
    listNodes = tk.Listbox(frame, width=100, yscrollcommand=scrollbar.set, font=("Helvetica", 12))
    listNodes.pack(expand=True, fill=tk.Y)
    scrollbar.config(command=listNodes.yview)
    
    listNodes.insert(tk.END, 'Nombur of iterations is :'+str(iteration))
    listNodes.insert(tk.END, 'values of vector R is :')
    for x in PR_vector_next:
        listNodes.insert(tk.END, x)
    
    # display a Menu 
    window.config(menu = menubar)
    window.mainloop()  

#______________________________________________ calculate a graph ______________________________________  
def create_graph(matrix_nodes):
           
    # create a graph
    graph = nx.DiGraph()
    for i in range(len(matrix_nodes)):
        # node
        node = matrix_nodes[i][0]
        
        # add each node to graph
        graph.add_node(node)
        
        # out_links
        out_links = matrix_nodes[i][1]
        for link in out_links:
            # make link with node and its out_links by flèche
            graph.add_edge(node , link)
            
    nx.draw(graph ,with_labels = True)
    plt.savefig("graph.png")

#______________________________________________ main ____________________________________________________           
if __name__=='__main__':
    create_xml()
    create_csv()
    [iteration , PR_vector_next ] = algo_page_rank()
    Interface(iteration , PR_vector_next)


